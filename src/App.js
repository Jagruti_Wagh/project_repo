import { connect } from "react-redux";
import Main from "./Components/Main_redux";
import { bindActionCreators } from "redux";
import * as actions from "./Redux/actions";
import { withRouter } from "react-router";

function mapStateToProps(state) {
  return {
    posts: state.posts,
    comments: state.comments
  };
}

//this.props.posts

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

const App = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Main)
);

export default App;
