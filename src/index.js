//import ReactDOM from 'react-dom';
import React from "react";
import ReactDOM from "react-dom";
import "./styles/stylesheet.css";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import rootReducer from "./Redux/reducer";
import { Provider } from "react-redux";
import App from "./App";
import thunk from "redux-thunk";
import { database } from "./database/config";

//first you create your reducer (function that will manipulate the state)
//then you create the store (createStore) and pass the reducer to it
const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
