import _posts from "../Data/posts";
import { combineReducers } from "redux";

function comments(state = {}, action) {
  switch (action.type) {
    case "ADD_COMMENT":
      if (!state[action.postId]) {
        return { ...state, [action.postId]: [action.comment] };
      } else {
        return {
          ...state,
          [action.postId]: [...state[action.postId], action.comment]
        };
      }
    default:
      return state;
  }
}

//the first state is always returned by our application
//action -> some event that happened on our application

//"state = posts" this is an example of ES 6 default parameters
function posts(state = _posts, action) {
  switch (action.type) {
    case "REMOVE_POST":
      return [
        //return the array removing the clicked item
        ...state.slice(0, action.index),
        ...state.slice(action.index + 1)
      ];
    case "ADD_POST":
      return [...state, action.post];
    default:
      return state;
  }
}

const rootReducer = combineReducers({ posts, comments });

export default rootReducer;
